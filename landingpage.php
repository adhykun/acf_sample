<?php
/* Template Name: Home - Landing Page */
global $fxbulls;
get_header(); ?>
<style>
.row p, footer .footer-warning .warning-content, .blog-meta a {
	font-weight: normal;
}
</style>
<main class="page-content">
    <div class="homepage">
        <section class="banner" style="background-image:url('<?php the_field('background_s1'); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1><?php the_field('title_s1'); ?></h1>
                        <h5><?php the_field('description_s1'); ?></h5>
                        <a href="<?php the_field('button_link_s1'); ?>" class="btn btn-lg btn-success">
                          <?php the_field('button_s1'); ?>
                        </a>
                    </div>
                    <div class="col-lg-12">
                        <a href="#benefit" class="scroll-down"></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="benefit bg-gray py-50" id="benefit">
            <div class="container">
                <div class="row features-1">
										<?php if( have_rows('features') ):
											while ( have_rows('features') ) : the_row();
												echo '
												<div class="col-lg-3 col-md-6">
														<figure>
																<div class="img">
																		<img src="'.get_sub_field('features_icon').'" alt="">
																</div>
																<figcaption>
																		<h6>'.get_sub_field('features_title').'</h6>
																		<p>'.get_sub_field('features_description').'</p>
																</figcaption>
														</figure>
												</div>
												';
											endwhile;
										endif; ?>

                </div>
            </div>
        </section>

        <section class="pairs">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <ul class="tab-chart nav justify-content-center" role="tablist">
                          <?php
                          if( have_rows('chart_tab') ):
                              while ( have_rows('chart_tab') ) : the_row();
                              $i = 0; ?>

                              <li class="nav-item">
                                  <a class="nav-link" data-target="#tabChart-<?php echo $i; ?>" data-toggle="tab" role="tab" aria-selected="true"><?php the_sub_field('tab_title'); ?></a>
                              </li>

                          <?php
                          $i ++; endwhile;
                          else :
                              // no rows found
                          endif;
                          ?>
                        </ul>


                        <div class="tab-content">
                          <?php
                          if( have_rows('chart_tab') ):
                              while ( have_rows('chart_tab') ) : the_row();
                              $i = 0; ?>

                              <div class="tab-pane fade" id="tabChart-<?php echo $i; ?>" role="tabpanel">
                                  <div class="row">
                                    <div class="col-lg-4">
                                        <h4>
                                            <?php the_sub_field('title'); ?>
                                        </h4>
                                        <p><?php the_sub_field('text'); ?></p>
                                        <div class="leverage">
                                            <h4>
                                                <small><?php the_sub_field('blue_box_small_text'); ?></small>
                                                <?php the_sub_field('blue_box_text'); ?>
                                            </h4>
                                        </div>
                                    </div>
																		<div class="col-lg-8">
																			<div class="owl-pairs owl-carousel owl-theme">
																			
																					<div class="item">
																							<figure>
																									<div class="cover" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/flag/card/gbpusd.png');"></div>
																									<div class="flags">
																											<img src="<?php echo get_template_directory_uri(); ?>/assets/img/flag/double/space/gbpusd.png" alt="">
																									</div>
																									<figcaption>
																											<h6>GBPUSD</h6>
																											<p>
																													<small>Today’s Bid/Ask Price</small>
																													1.27305/1.27373
																											</p>
																											<div class="chart">
																													<p>
																															<small>1D change</small>
																															<span class="text-danger"><i class="fas fa-caret-down"></i> 0.38%</span>
																													</p>
																													<div class="get-real-chart">

																													</div>
																											</div>
																											<a href="#" class="btn btn-default btn-block">
																													<b>0.067%</b> <small>spread</small>
																											</a>
																									</figcaption>
																							</figure>
																					</div>

																			</div>
																		</div>
                                  </div>
                              </div>

                          <?php
                          $i ++; endwhile;
                          else :
                              // no rows found
                          endif;
                          ?>

                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="ps">
                            <?php the_field('notation'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-gray advantages">
            <div class="container">
                <div class="title text-center">
                    <h3><?php the_field('titlte_s4'); ?></h3>
                    <h5><?php the_field('description_s4'); ?></h5>
                </div>

                <div class="row features-1">
										<?php if( have_rows('features_s4') ):
											$i = 1;
											while ( have_rows('features_s4') ) : the_row();
												echo '
												<div class="col-lg-4 col-md-6">
														<figure>
																<div class="img">
																		<img src="'.get_sub_field('features_icon_s4').'" alt="">
																</div>
																<figcaption>
																		<h6>'.get_sub_field('features_title_s4').'</h6>
																		<p>'.get_sub_field('features_description_s4').'</p>
																</figcaption>
														</figure>
												</div>
												';
											$i++;
											endwhile;
										endif; ?>
                </div>
            </div>
        </section>

        <section class="trading-platform">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title mb-30">
                            <h3><?php the_field('title_s5'); ?></h3>
                            <h5><?php the_field('sec_title_s5'); ?></h5>
                        </div>
												<?php the_field('description_s5'); ?>
                    </div>
                </div>
            </div>

            <div class="right-img">
                <img src="<?php the_field('background_s5'); ?>" alt="">
            </div>

            <div class="container">

                <div class="row platform-os justify-content-center">
                    <div class="col-lg-11">
                        <div class="row align-items-center">
                            <div class="col-lg-4 col-md-6">
                                <div class="metatrader">
                                    <img src="<?php the_field('logo_s5'); ?>" alt="">
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p><?php the_field('for_desktop_label'); ?></p>
                                <div class="os">
																<?php if( have_rows('icon_desktop') ):
																	$i = 1;
																	while ( have_rows('icon_desktop') ) : the_row();
																		echo '
																		<img src="'.get_sub_field('i_desktop').'" alt="">
																		';
																	$i++;
																	endwhile;
																endif; ?>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <p><?php the_field('for_browser_label'); ?></p>
                                <div class="os">
																<?php if( have_rows('icon_browser') ):
																	$i = 1;
																	while ( have_rows('icon_browser') ) : the_row();
																		echo '
																		<img src="'.get_sub_field('i_browser').'" alt="">
																		';
																	$i++;
																	endwhile;
																endif; ?>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-6">
                                <p><?php the_field('for_mobile_label'); ?></p>
                                <div class="os">
																<?php if( have_rows('icon_mobile') ):
																	$i = 1;
																	while ( have_rows('icon_mobile') ) : the_row();
																		echo '
																		<img src="'.get_sub_field('i_mobile').'" alt="">
																		';
																	$i++;
																	endwhile;
																endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about" style="background-image: url('<?php the_field('background_s6'); ?>');">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <div class="title mb-30">
                            <h3><?php the_field('title_s6'); ?></h3>
                            <h5><?php the_field('second_title_s6'); ?></h5>
                        </div>

                        <?php the_field('description_s6'); ?>

                        <a href="<?php the_field('button_link_s6'); ?>" class="btn btn-outline-light"><?php the_field('button_label_s6'); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="pricing">
            <div class="container">
                <div class="title text-center">
                    <h3><?php the_field('title_s7'); ?></h3>
                    <h5><?php the_field('description_s7'); ?></h5>
                </div>

                <div class="pricing-1">
                    <div class="row align-items-end">
                        <div class="col-lg-3 col-md-6">
														<?php if( have_rows('accout_type_cent', 692) ):
															$i = 1;
															while ( have_rows('accout_type_cent', 692) ) : the_row();
																echo '
																<figure>
																		<div class="cover"></div>
																		<div class="icon bg-success">
																				<img src="'.get_sub_field('icon').'" alt="">
																		</div>
																		<figcaption>
																				<h4>'.get_sub_field('title').'</h4>
																				<p>'.get_sub_field('short_desc').'</p>
																				<div class="deposit">
																						<p>'.get_sub_field('min_deposit_title').'</p>
																						<h4>'.get_sub_field('min_deposit_value').'</h4>
																				</div>
																				<a href="'.get_sub_field('button_open_account_link').'" class="btn btn-outline-primary btn-block">'.get_sub_field('button_open_account_label').'</a>
																		</figcaption>
																</figure>
																';
															$i++;
															endwhile;
														endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-6">
														<?php if( have_rows('accout_type_standard', 692) ):
															$i = 1;
															while ( have_rows('accout_type_standard', 692) ) : the_row();
																echo '
																<figure>
																		<div class="cover"></div>
																		<div class="icon bg-secondary">
																				<img src="'.get_sub_field('icon').'" alt="">
																		</div>
																		<figcaption>
																				<h4>'.get_sub_field('title').'</h4>
																				<p>'.get_sub_field('short_desc').'</p>
																				<div class="deposit">
																						<p>'.get_sub_field('min_deposit_title').'</p>
																						<h4>'.get_sub_field('min_deposit_value').'</h4>
																				</div>
																				<a href="'.get_sub_field('button_open_account_link').'" class="btn btn-outline-primary btn-block">'.get_sub_field('button_open_account_label').'</a>
																		</figcaption>
																</figure>
																';
															$i++;
															endwhile;
														endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-6">
														<?php if( have_rows('accout_type_pro', 692) ):
															$i = 1;
															while ( have_rows('accout_type_pro', 692) ) : the_row();
																echo '
																<figure class="popular">
																		<div class="cover">
																				<h6>Most Popular</h6>
																		</div>
																		<div class="icon bg-warning">
																				<img src="'.get_sub_field('icon').'" alt="">
																		</div>
																		<figcaption>
																				<h4>'.get_sub_field('title').'</h4>
																				<p>'.get_sub_field('short_desc').'</p>
																				<div class="deposit">
																						<p>'.get_sub_field('min_deposit_title').'</p>
																						<h4>'.get_sub_field('min_deposit_value').'</h4>
																				</div>
																				<a href="'.get_sub_field('button_open_account_link').'" class="btn btn-outline-primary btn-block">'.get_sub_field('button_open_account_label').'</a>
																		</figcaption>
																</figure>
																';
															$i++;
															endwhile;
														endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-6">
														<?php if( have_rows('accout_type_ecn', 692) ):
															$i = 1;
															while ( have_rows('accout_type_ecn', 692) ) : the_row();
																echo '
																<figure>
																		<div class="cover"></div>
																		<div class="icon bg-primary">
																				<img src="'.get_sub_field('icon').'" alt="">
																		</div>
																		<figcaption>
																				<h4>'.get_sub_field('title').'</h4>
																				<p>'.get_sub_field('short_desc').'</p>
																				<div class="deposit">
																						<p>'.get_sub_field('min_deposit_title').'</p>
																						<h4>'.get_sub_field('min_deposit_value').'</h4>
																				</div>
																				<a href="'.get_sub_field('button_open_account_link').'" class="btn btn-outline-primary btn-block">'.get_sub_field('button_open_account_label').'</a>
																		</figcaption>
																</figure>
																';
															$i++;
															endwhile;
														endif; ?>
                        </div>
                    </div>
                </div>

                <a href="<?php the_field('full_comparison_link'); ?>" class="btn btn-default btn-block mt-20"><?php the_field('full_comparison_link_title'); ?></a>
            </div>
        </section>

        <section class="partner" style="background-image: url('<?php the_field('background_s8'); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title mb-30">
                            <h3><?php the_field('title_s8'); ?></h3>
                            <h5><?php the_field('second_title_s8'); ?></h5>
                        </div>

                        <?php the_field('description_s8'); ?>

                        <a href="<?php the_field('button_link_s8'); ?>" class="btn btn-primary"><?php the_field('button_label_s8'); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="recent-posts">
            <div class="container">
                <div class="title text-center">
                    <h3><?php the_field('title_s9'); ?></h3>
                    <h5><?php the_field('description_s9'); ?></h5>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-posts">
								<?php
								$loop = new WP_Query( array (
														 'posts_per_page'   => 5,
														 'post_type' => 'post'
													)
								 );
								if (have_posts()) :
									while ($loop->have_posts() ) :
										$loop->the_post();

										$categories = get_the_category($post->ID);
										if ( ! empty( $categories ) ) {
												$cat = '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
										}
										?>

										<div class="media">
												<div class="img">
														<?php the_post_thumbnail('medium', array('class' => '')); ?>
												</div>
												<div class="media-body">
														<h6 class="blog-title">
																<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h6>
														<div class="blog-meta">
																<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>">By <?php the_author(); ?></a>
																<?php echo $cat;?>
																<p><?php echo get_the_time('M j, Y');?> &middot; <?php echo get_the_time('h:i a');?></p>
														</div>
														<div class="blog-excerpt">
																<p>
																	<?php
																	$p = get_post( $post->ID );
																	$p = $p->post_content;
																	echo ntc_excerpt($p);
																	?>
																		<a href="<?php the_permalink(); ?>">Read more</a>
																</p>
														</div>
												</div>
										</div>
							<?php
								endwhile;
								wp_reset_postdata();
							else: ?>
								<p><?php _e('Maaf posting tidak tersedia'); ?></p>
							<?php endif;?>

                        </div>
                        <a href="<?php the_field('view_all_post_text'); ?>" class="btn btn-default btn-block mb-40"><?php the_field('view_all_post_text'); ?></a>
                    </div>
                    <div class="col-lg-4">
                        <div class="row">
														<?php if( have_rows('side_1') ):
															$i = 1;
															while ( have_rows('side_1') ) : the_row();
																echo '
																<div class="col-lg-12 col-md-6">
																		<div class="widget bg-dark"
																		style="background-image:url('.get_sub_field('background').');">
																				<h4>'.get_sub_field('title').'</h4>
																				'.get_sub_field('description').'
																		</div>
																</div>
																';
															$i++;
															endwhile;
														endif; ?>
														<?php if( have_rows('side_2') ):
															$i = 1;
															while ( have_rows('side_2') ) : the_row();
																echo '
																<div class="col-lg-12 col-md-6">
																		<div class="widget bg-dark" style="background-image:url('.get_sub_field('background').');">
																				<h4>'.get_sub_field('title').'</h4>
																				'.get_sub_field('description').'
																		</div>
																</div>
																';
															$i++;
															endwhile;
														endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

		<section class="footer-cta">
				<div class="container">
						<div class="row justify-content-center">
								<div class="col-md-10">
										<h3><?php the_field('title_s10'); ?></h3>
										<div class="cta-btn">
												<div class="item">
														<a href="<?php the_field('button_link_1'); ?>" class="btn btn-lg btn-success">
																<?php the_field('button_label_1'); ?>
														</a>
												</div>
												<div class="item">
														<a href="<?php the_field('button_link_2'); ?>" class="btn btn-lg btn-outline-light">
																<?php the_field('button_label_2'); ?>
														</a>
														<p><?php the_field('risk_note'); ?></p>
												</div>
										</div>
								</div>
						</div>
				</div>
		</section>
</main> <!-- main -->

<?php get_footer(); ?>
